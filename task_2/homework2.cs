﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System.Threading;


namespace Selenium_Test_Viola
{
    class homework2
    {
        IWebDriver driver;

        [SetUp]
        public void Initialize()
        {
            driver = new ChromeDriver();// Starting a Chrome Browser
        }

        [Test]
        public void Test()
        {
            //---------1. Зайти на Github.com-------------
            driver.Url = "Github.com";

            //---------2. В Why GitHub? выбрать Actions-----------
            Actions actions = new Actions(driver);
            actions.MoveToElement(driver.FindElement(By.XPath("/html/body/div[1]/header/div/div[2]/nav/ul/li[1]/details/summary"))).Build().Perform();
            driver.FindElement(By.XPath("/html/body/div[1]/header/div/div[2]/nav/ul/li[1]/details/div/ul[1]/li[4]/a")).Click();

            //----------3. В поле поиска вбить Selenium и нажать enter-----
            IWebElement searchbar = driver.FindElement(By.XPath("/html/body/div[1]/header/div/div[2]/div[2]/div/div/div/form/label/input[1]"));
            searchbar.SendKeys("Selenium");//ищем слово selenium
            searchbar.SendKeys(Keys.Return);//нажимаем на enter

            //-----------4. Открыть ссылку на первый репозиторий в новой табе-----
            //-----------5. Перейти на новую табу-----------------
            IWebElement firstOption = driver.FindElement(By.XPath("/html/body/div[4]/main/div/div[3]/div/ul/li[1]/div[1]/h3/a"));//первый репозиторий
            Actions newTab = new Actions(driver);
            newTab.KeyDown(OpenQA.Selenium.Keys.Control).KeyDown(OpenQA.Selenium.Keys.Shift).Click(firstOption)
                .KeyUp(OpenQA.Selenium.Keys.Control).KeyUp(OpenQA.Selenium.Keys.Shift)
                .Build()
                .Perform();//открываем в новой вкладке и переходим на нее

            //-----------6.Перезагрузить страницу--------
            driver.Navigate().Refresh();

            //-----------7. Проверить что вы всё ещё находитесь на странице того же репозитория (имя репозитория = "selenium")
            string whichRepositoryIAmOn = driver.FindElement(By.XPath("/html/body/div[4]/div/main/div[1]/div/h1/strong/a")).Text;
            string expectedString = "selenium";
            Assert.AreEqual(expectedString, whichRepositoryIAmOn);

            //-----------8. Закрыть текущую вкладку---
            driver.Close();

            //----------9. Перейти по ссылке "https://selenium.dev/selenium/docs/api/dotnet/"
            driver.Url = "https://selenium.dev/selenium/docs/api/dotnet/";

            //---------10. Получить код страницы и записать его в файл под названием pageSource.html----
            string pagesource = driver.PageSource;
            var newFile = new StreamWriter("pageSource.html");
            newFile.WriteLine(pagesource);
            newFile.Close();

            //--------11. Проверить что код страницы содержит строку "IHasInputDevices Interface"---
            string inputString = "IHasInputDevices Interface";
            Assert.AreEqual(pagesource, inputString);
        }



        public void Close()
        {
            driver.Quit();
        }
    }
}
