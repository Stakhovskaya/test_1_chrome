﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System.Threading;

namespace Selenium_Test_Viola
{
    class homework_Task1
    {
        IWebDriver driver; 

        [SetUp]
        public void Initialize()
        {
            driver = new ChromeDriver();// Starting a Chrome Browser
        }

        [Test]
        public void Test()
        {
           
            driver.Url = "https://html.com/tags/iframe/";
            String PageURL = driver.Url;// Storing URL in String variable
            Console.WriteLine("URL of the page is : " + PageURL);// Printing url on console

            //-----------finding element,containing iframe, using XPath--------------
            IWebElement element = driver.FindElement(By.XPath("/html/body/div[1]/div/div/main/div[3]/article/div/div[4]/iframe"));

            //----------scrolling to IFrame class, which contains youtube video-------
            Actions actions = new Actions(driver);
            actions.MoveToElement(element).Build().Perform();
            Thread.Sleep(3000);

            //-----------swiching to an IFrame-----------------
            driver.SwitchTo().Frame(element);

            //----------getting the name of the video------
            actions.MoveToElement(driver.FindElement(By.XPath("/html/body/div/div/div[3]/div[2]/div/a"))).Build().Perform();
            Thread.Sleep(3000);

           //----comparing the video's title with the actual title--------
            var videoName = driver.FindElement(By.XPath("/html/body/div/div/div[3]/div[2]/div/a"));
            Assert.IsTrue(videoName.Displayed);
            Assert.AreEqual(videoName.Text.ToLower(), "Massive volcanoes & Flamingo colony - Wild South America - BBC".ToLower());

            //---clicking on the video's title------
            actions.MoveToElement(videoName).Click().Perform();

            //-------making sure "Проверить что элемент с id = country-code имеет текст = BY"

            string makeSure = driver.FindElement(By.Id("country-code")).Text;
            string expectedString = "BY";
            Assert.AreEqual(expectedString, makeSure);
        }
        [TearDown]
        public void Close()
        {
            driver.Quit();
        }

    }
}

